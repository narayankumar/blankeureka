<div id="wrapper">
  <?php include 'assets/includes/header.inc'; ?>
  <div id="main" class="container_12">
    <div class="grid_10">
      <h1><?php print $title; ?></h1>
      <?php $tabs && print render($tabs); ?>
      <?php print render($page['content']); ?>
    </div>
    <div class="grid_2 aside">
      <?php
        $page['sidebar_first'] && print render($page['sidebar_first']);
      ?>
    </div>
  </div>
</div>
<?php include 'assets/includes/footer.inc';