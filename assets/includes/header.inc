<div id="header">
  <div class="container_12">
    <div class="grid_3" id="ss_logo">
      <?php
        if (!$is_front) {
          print '<a href="/">';
        }

        print '<span>Eureka Submissions</span>';

        if (!$is_front) {
          print '</a>';
        }
      ?>
    </div>
    <div class="grid_7">
      <ul id="nav">
        <li id="nav_home">
          <a href="/">Home</a>
        </li>
        <li id="nav_recommended">
          <a href="/flagged-submissions">Recommended</a>
        </li>
        <li id="nav_chosen">
          <a href="/chosen-ones">Chosen</a>
        </li>
        <li id="nav_resources">
          <a href="/resources">Resources</a>
        </li>
        <li id="nav_contact">
          <a href="/contact">Contact</a>
        </li>
      </ul>
    </div>
    <div class="grid_2">
      <img class="metal" src="sites/eureka.metal-hq.com/themes/blankeureka/assets/images/metallogo.jpg">
    </div>
  </div>
</div>
