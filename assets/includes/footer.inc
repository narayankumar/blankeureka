<div id="footer">
  <div class="container_12">
    <div class="grid_4">
      &copy; <?php print date('Y'); ?> <a href="http://metal-hq.com">Metal Communications</a>. All rights reserved.
    </div>
    <div class="grid_4">
      Powered by <a href="http://metal-hq.com/page/what-metalapp" title="MetalApp" id="metalapp">Drupal-based MetalApp</a> (version 4.16)
    </div>
    <div class="grid_4 align_right">
      Exclusive use of Eureka Mobile Advertising and associates
    </div>
  </div>
</div>