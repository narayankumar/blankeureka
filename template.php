<?php

function blankeureka_section() {
  $section_path = explode('/', request_uri());
  $section_name = $section_path[1];
  $section_q = strpos($section_name, '?');

  if ($section_q) {
    $section_name = substr($section_name, 0, $section_q);
  }

  switch ($section_name) {
    case '':
      return 'section_home';
      break;
    case 'submissions':
      return 'section_home';
      break;
    case 'flagged-submissions':
      return 'section_flagged';
      break;
    case 'chosen-ones':
      return 'section_chosen';
      break;
    case 'resources':
      return 'section_resources';
      break;
    case 'contact':
      return 'section_contact';
      break;
    case 'search':
      return 'section_search';
      break;
    case 'user':
      return 'section_user';
      break;
    case 'users':
      return 'section_user';
      break;
    case 'filter':
      return 'section_filter';
      break;
    case 'admin':
      return 'section_admin';
      break;
    default:
      return 'section_404';
  }
}

// Purge needless XHTML stuff.
function blankeureka_process_html_tag(&$vars) {
  $el = &$vars['element'];

  // Remove type="..." and CDATA prefix/suffix.
  unset($el['#attributes']['type'], $el['#value_prefix'], $el['#value_suffix']);

  // Remove media="all" but leave others unaffected.
  if (isset($el['#attributes']['media']) && $el['#attributes']['media'] === 'all') {
    unset($el['#attributes']['media']);
  }
}

// Minify HTML output.
function blankeureka_process_html(&$vars) {
  $before = array(
    "/>\s\s+/",
    "/\s\s+</",
    "/>\t+</",
    "/\s\s+(?=\w)/",
    "/(?<=\w)\s\s+/"
  );

  $after = array('> ', ' <', '> <', ' ', ' ');

  // Page top.
  $page_top = $vars['page_top'];
  $page_top = preg_replace($before, $after, $page_top);
  $vars['page_top'] = $page_top;

  // Page content.
  if (!preg_match('/<pre|<textarea/', $vars['page'])) {
    $page = $vars['page'];
    $page = preg_replace($before, $after, $page);
    $vars['page'] = $page;
  }

  // Page bottom.
  $page_bottom = $vars['page_bottom'];
  $page_bottom = preg_replace($before, $after, $page_bottom);
  $vars['page_bottom'] = $page_bottom . drupal_get_js('footer');
}

  // Remove height and width from Drupal images.
  function blankeureka_preprocess_image(&$variables){
    foreach(array('width','height') as $key){
      unset($variables[$key]);
    }
  }