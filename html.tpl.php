
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=edge, chrome=1" />
<title><?php
  if (blankeureka_section() === 'section_home') {
    print 'Eureka Submissions';
  }
  else {
    print $head_title;
  }
?></title>

<meta name="author" content="Metal Communications">
<meta name="description" content="An intranet for collection and selection of Interesting Snippets for publication on the Eureka mobile system" />
<link rel="shortcut icon" type="image/x-icon" href="/sites/all/themes/blankeureka/assets/images/favicon.ico" />
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald" />
<?php print $styles; ?>
</head>
<body id="<?php print blankeureka_section(); ?>">
<?php
  print $page_top;
  print $page;
  print $scripts;
  print $page_bottom;
?>
</body>
</html>